<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1)H`?!wi<$p1rx28lXxJ[)SggPR[U 8eSFnNshzc>.k%mbmu:c<O|#A1#$<bK#]H');
define('SECURE_AUTH_KEY',  '-wF/;mOx3qS[)8>PC6l(+DGfp;Eb-t,t:1]%=7l`Bi_Tck:o^Mcom[?&l8 O#W1I');
define('LOGGED_IN_KEY',    'F,Zbi~|k3}Kd}<Cmfyw$8li(QAlMge*l4>6xUw|:WokyKVyRw-w|r)y1ZeXa[d##');
define('NONCE_KEY',        'XP,2/ L5)^I9-]To=&BscklvyX6kn8Ub=U9KZ:FT^e$P2>#/_)+AKo6|p?kU(NzU');
define('AUTH_SALT',        'j%MOuq;X=~Yi7ITGZ{j(zn_X%1j@gA=ldZb7%ZS+iUl}ZdOz[*9fpiPKkSBq.(Bt');
define('SECURE_AUTH_SALT', '(N*9SFybR,*Pf*L6H<8H&ZJ8U_iUmtnVBoE.iu_zCF.a#i=S75WR6sfS;iE1[B<w');
define('LOGGED_IN_SALT',   '7yG7>X~8!}t*n|v%1c)xU>fW1]|={1FL2vF=~q4:]9yy.W_yE<9}}E}Y7xFJ@j||');
define('NONCE_SALT',       'S 1^<cwDZvjEbeZl; Br|7U1r^`w|,I<] 5-K570c1[EGD^%tTO|OJzYg1XTJx9I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
