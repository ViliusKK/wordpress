<?php get_header(); ?>


<div class="container-fluid ">
    <div class="row text-center">
        <div class="col-12">
            <h1>Our Services</h1>
        </div>
        <hr class="sericeHorizontalLine">
        <div class="col-12">
        </div>
    </div>
</div>

<!--- four Column Section -->
<div class="container-fluid fourServicesIcons">
    <div class="row text-center ">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <i><img src="img/icon-3.png"></i>
            <h3>Logo design</h3>
            <hr class="ourServiceLogoLine">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting let.
                Lorem Ipsum has been the industry.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <i><img src="img/icon-1.png"></i>
            <h3>web design</h3>
            <hr class="ourServiceLogoLine">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting let.
                Lorem Ipsum has been the industry.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <i><img src="img/icon-2.png"></i>
            <h3>branding</h3>
            <hr class="ourServiceLogoLine">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting let.
                Lorem Ipsum has been the industry.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <i><img src="img/icon-4.png"></i>
            <h3>illustrations</h3>
            <hr class="ourServiceLogoLine">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting let.
                Lorem Ipsum has been the industry.</p>
        </div>
    </div>
</div>

<!--- six Column Section -->

<div class="container-fluid bgColorBrands">
    <div class="row text-center">
        <div class="col-12">
            <h1 class="brandsTitle">recent brands we've worked with</h1>
        </div>
        <hr class="brandsHorizontalLine">
    </div>
    <div class="row text-center img-opacity">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <i><img src="img/Layer%206.png"></i>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <i><img src="img/Layer%207.png"></i>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <i><img src="img/Layer%208.png"></i>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <i><img src="img/Layer%209.png"></i>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <i><img src="img/Layer%2010.png"></i>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <i><img src="img/Layer%2011.png"></i>
        </div>
    </div>
</div>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="post-header">
                <div class="date"><?php the_time( 'M j y' ); ?></div>
                <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                <div class="author"><?php the_author(); ?></div>
            </div><!--end post header-->
            <div class="entry clear">
                <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
                <?php the_content(); ?>
                <?php edit_post_link(); ?>
                <?php wp_link_pages(); ?> </div>
            <!--end entry-->
            <div class="post-footer">
                <div class="comments"><?php comments_popup_link( 'Leave a Comment', '1 Comment', '% Comments' ); ?></div>
            </div><!--end post footer-->
        </div><!--end post-->
    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
    <div class="navigation index">
        <div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
        <div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
    </div><!--end navigation-->
<?php else : ?>
<?php endif; ?>



<?php get_footer(); ?>
